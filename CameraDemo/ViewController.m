//
//  ViewController.m
//  CameraDemo
//
//  Created by taitanxiami on 2016/10/15.
//  Copyright © 2016年 taitanxiami. All rights reserved.
//

#import "ViewController.h"
#import <CommonCrypto/CommonCryptor.h>
#import <Foundation/Foundation.h>
@interface ViewController ()<UIImagePickerControllerDelegate>

{
    UIImagePickerController *_picker;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _picker = [[UIImagePickerController alloc]init];
    
//    _picker.delegate = self;
//    _picker.allowsEditing = YES;
//    _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    _picker.allowsEditing  = NO;
    
    
    NSString *result = nil;
    
        result = [self encryptUseDES:@"56422" key:@"dg4das516488dfi8" andiv:@"DES"];
}

- (NSData *)DESEncrypt:(NSData *)data WithKey:(NSString *)key
{
    char keyPtr[kCCKeySizeAES256+1];
    bzero(keyPtr, sizeof(keyPtr));
    
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [data length];
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeDES,
                                          NULL,
                                          [data bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free(buffer);
    return nil;
}

//nsdata转成16进制字符串
- (NSString*)stringWithHexBytes2:(NSData *)sender {
    static const char hexdigits[] = "0123456789ABCDEF";
    const size_t numBytes = [sender length];
    const unsigned char* bytes = [sender bytes];
    char *strbuf = (char *)malloc(numBytes * 2 + 1);
    char *hex = strbuf;
    NSString *hexBytes = nil;
    
    for (int i = 0; i<numBytes; ++i) {
        const unsigned char c = *bytes++;
        *hex++ = hexdigits[(c >> 4) & 0xF];
        *hex++ = hexdigits[(c ) & 0xF];
    }
    
    *hex = 0;
    hexBytes = [NSString stringWithUTF8String:strbuf];
    
    free(strbuf);
    return hexBytes;
}
-(NSString *) encryptUseDES:(NSString *)clearText key:(NSString *)key andiv:(NSString *)iv
{
    //这个iv 是DES加密的初始化向量，可以用和密钥一样的MD5字符
    NSData * date = [iv dataUsingEncoding:NSUTF8StringEncoding];
    NSString *ciphertext = nil;
    NSUInteger dataLength = [clearText length];
    NSData *textData = [clearText dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char buffer[1024];
    memset(buffer, 0, sizeof(char));
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,//加密模式 kCCDecrypt 代表解密
                                          kCCAlgorithmDES,//加密方式
                                          kCCOptionPKCS7Padding,//填充算法
                                          [key UTF8String], //密钥字符串
                                          kCCKeySizeDES,//加密位数
                                          [date bytes],//初始化向量
                                          [textData bytes]  ,
                                          dataLength,
                                          buffer, 1024,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        NSLog(@"DES加密成功");
        NSData *data = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesEncrypted];
        
        return   [self stringWithHexBytes2:data];
//        Byte* bb = (Byte*)[data bytes];
//        ciphertext = [Base64 parseByteArray2HexString:bb];
    }else{
        NSLog(@"DES加密失败");
    }
    return ciphertext;
}
- (IBAction)showCanma:(id)sender {
    [self presentViewController:_picker animated:YES completion:nil];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSLog(@"拍照完成并使用");
    
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    //压缩图片
    NSData *fileData = UIImageJPEGRepresentation(image, 0.1);
    
    //size
    
    NSLog(@"%.2f",[fileData length] / 1024.00 /1024.00);
    //保存图片至相册
//    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    //上传图片
//    [self uploadImageWithData:fileData];
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    NSLog(@"cancle");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 图片保存完毕的回调
- (void) image: (UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo: (void *)contextInf{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
