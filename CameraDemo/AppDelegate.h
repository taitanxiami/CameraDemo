//
//  AppDelegate.h
//  CameraDemo
//
//  Created by taitanxiami on 2016/10/15.
//  Copyright © 2016年 taitanxiami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

